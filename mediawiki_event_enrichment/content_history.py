import os
from concurrent.futures import ThreadPoolExecutor, wait
from typing import Optional

import requests
from eventutilities_python.stream import http_process_function, stream_manager
from eventutilities_python.stream.error import error_schema_version
from eventutilities_python.utils import setup_logging
from jsonargparse import ArgumentParser, Namespace

from mediawiki_event_enrichment.mwapi.action import (
    create_retry_predicate_for_event,
    mediawiki_api_endpoint_template_default,
)
from mediawiki_event_enrichment.mwapi.content import assert_allowed_revision_size
from mediawiki_event_enrichment.mwapi.redirect import get_page_redirect_response
from mediawiki_event_enrichment.utils.config import get_config

from mediawiki_event_enrichment.page_content_change import (
    enrich_with_content,
    retry_on_get_page_content_response,
)

job_name = "mw_dump_rev_content_reconcile_enrich"
"""
Name of this application/job; will be used as default Flink job_name.
"""

source_stream_default = "mediawiki.dump.revision_content_history_reconcile:1.2.0"
"""
Default source EventStreamDescriptor.
"""

sink_stream_default = "mediawiki.dump.revision_content_history_reconcile.enriched:1.2.0"
"""
Default sink EventStreamDescriptor.
"""

error_sink_stream_default = f"{job_name}.error:{error_schema_version}"
"""
Default error_sink EventStreamDescriptor.
"""

arg_parser = ArgumentParser(
    prog="mediawiki-dump-content-history-enrich",
    description="Enriches mediawiki/page/change events with raw wiki content and redirect using MW API:Revisions",
)
arg_parser.add_argument(
    "--enrich.mediawiki_api_endpoint_template",
    default=mediawiki_api_endpoint_template_default,
    help="A python format string template for the MW Action API endpoint to use.  "
    "Only `domain` is supported in the template, and will be extracted from the input event.",
)
arg_parser.add_argument(
    "--enrich.ssl_ca_bundle_path",
    help="Path to the SSL CA bundle to use when making HTTPS requests.",
)
arg_parser.add_argument(
    "--enrich.request_timeout",
    type=int,
    default=1,
    help="MW API request timeout in seconds",
)
arg_parser.add_argument(
    "--enrich.max_rev_size",
    type=int,
    default=8_000_000,
    help="Max allowed size (bytes) of a revision `rev_size`. Events whose "
    "payload exceeds max_rev_size won't be enriched",
)
# Since we will be using @http_process_function to decorate
# our http enrich process function, add params for it.
arg_parser.add_function_arguments(
    http_process_function,
    nested_key="http_session",
)

arg_parser_defaults = {
    "enrich.mediawiki_api_endpoint_template": mediawiki_api_endpoint_template_default,
    "stream_manager.job_name": job_name,
    "stream_manager.source.stream": source_stream_default,
    "stream_manager.sink.stream": sink_stream_default,
    "stream_manager.error_sink.stream": error_sink_stream_default,
    "http_session.pool_maxsize": 12,
}


def retry_on_get_page_redirect_response(
    event: dict,
    http_session: requests.Session,
    user_agent: str,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    wait_on_retry_seconds: int = 5,
    retry_on_event_time_lag_seconds: int = 10,
) -> dict:
    # schema specific access pattern
    page_id = event["page"]["page_id"]
    domain = event["meta"]["domain"]

    retry_predicate = create_retry_predicate_for_event(
        event,
        retry_on_event_time_lag_seconds,
    )

    response = get_page_redirect_response(
        page_id=page_id,
        domain=domain,
        user_agent=user_agent,
        http_session=http_session,
        mediawiki_api_endpoint_template=mediawiki_api_endpoint_template,
        ssl_ca_bundle_path=ssl_ca_bundle_path,
        request_timeout=request_timeout,
        wait_on_retry_seconds=wait_on_retry_seconds,
        retry_predicate=retry_predicate,
    )

    response.raise_for_status()

    return response.json()


def enrich_with_redirect_info(event: dict, response_body: dict) -> dict:
    """
    Enrich a page_change event with redirect information.

    :param event: a dict representing an event with page_change schema.
    :response_body: the body of a successful MediaWiki (action) api request.
    :return: the input event, with a newly enriched `created_redirect_page` field.
    """
    if "redirects" in response_body.get("query", {}):
        pages = response_body["query"]["pages"]
        redirect_page_title = response_body["query"]["redirects"][0]["to"]
        for page in pages:
            # Assumption: we query action with redirects=1 (follow redirects)
            # In case the pages array contains more than on page,
            # select the one whose title matches redirect_page_title.
            if page["title"] == redirect_page_title:
                event["created_redirect_page"] = {}
                event["created_redirect_page"]["page_id"] = int(page["pageid"])
                event["created_redirect_page"]["page_title"] = redirect_page_title
                event["created_redirect_page"]["namespace_id"] = int(page["ns"])
                event["page"]["is_redirect"] = True
                return event
    return event


def enrich_page_change_with_redirect_and_content(
    event: dict,
    http_session: requests.Session,
    user_agent: str,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    max_rev_size: int = 8_000_000,
    wait_on_retry_seconds: int = 5,
    retry_on_event_time_lag_seconds: int = 10,
):
    assert_allowed_revision_size(event, max_rev_size)

    with ThreadPoolExecutor(max_workers=2) as executor:
        redirect_response_future = executor.submit(
            retry_on_get_page_redirect_response,
            event,
            http_session,
            user_agent,
            mediawiki_api_endpoint_template,
            ssl_ca_bundle_path,
            request_timeout,
            wait_on_retry_seconds,
            retry_on_event_time_lag_seconds,
        )

        content_response_future = executor.submit(
            retry_on_get_page_content_response,
            event,
            http_session,
            user_agent,
            mediawiki_api_endpoint_template,
            ssl_ca_bundle_path,
            request_timeout,
            wait_on_retry_seconds,
            retry_on_event_time_lag_seconds,
        )
        # If either future fails, we'll discard the event and publish to an error topic.
        # If both futures fail, we discard the event and log the exception from the future
        # that completed last.
        wait([redirect_response_future, content_response_future])

        event = enrich_with_redirect_info(event, redirect_response_future.result())
        event = enrich_with_content(event, content_response_future.result())

    return event


def main(config: Namespace = None):
    config = config or get_config(
        parser=arg_parser, parser_defaults=arg_parser_defaults
    )

    @http_process_function(**config.http_session)
    def enrich(event: dict, http_session: requests.Session) -> dict:
        # Remove meta.id so it can be regenerated from eventutilities.
        # TODO: upstream this operation?
        if "id" in event["meta"]:
            event["meta"].pop("id", "")
        return (
            event
            if event["changelog_kind"] not in ["insert", "update"]
            else enrich_page_change_with_redirect_and_content(
                event=event,
                http_session=http_session,
                user_agent=job_name,
                **config.enrich,
            )
        )

    with stream_manager.from_config(config) as stream:
        # Filter out canary events
        stream.filter(
            lambda e: e["meta"]["domain"] != "canary",
            name="remove_canary_events",
        )

        # The mediawiki.page_change.v1 stream is produced
        # by eventgate to Kafka with a JSON blob key that
        # includes page_id and wiki_id.
        # TODO: nowadays stream partitioning info is configured in ESC.
        # we should update eventutilities-python to perform this operation
        # implicitly (for default kafka partition keys).
        stream.partition_by(lambda e: (e["wiki_id"], e["page"]["page_id"]))
        stream.process(enrich, name="enrich_page_change_with_redirect_and_content")
        stream.execute()


if __name__ == "__main__":
    setup_logging(level=os.environ.get("LOG_LEVEL", "INFO"))
    main()
