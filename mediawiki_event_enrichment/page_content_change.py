import os
import threading
from threading import Lock
from typing import Any, Dict, List

from eventutilities_python.stream import (
    http_process_function,
    load_config,
    stream_manager,
)
from eventutilities_python.stream.error import error_schema_version
from eventutilities_python.utils import setup_logging
from jsonargparse import ArgumentParser, Namespace

from mediawiki_event_enrichment.mwapi.action import *
from mediawiki_event_enrichment.mwapi.content import (
    assert_allowed_revision_size,
    get_page_content_response,
)
from mediawiki_event_enrichment.utils.config import get_config
from mediawiki_event_enrichment.utils.logging import log_event_enrich_request_error

job_name = "mediawiki_page_content_change_enrich"

"""
Default MW API endpoint.  The default is to request page content from the 'meta.domain'
field host in the page_change event.
"""

source_stream_default = "mediawiki.page_change:1.2.0"
"""
Default source EventStreamDescriptor.
"""

sink_stream_default = "mediawiki.page_content_change:1.2.0"
"""
Default sink EventStreamDescriptor.
"""

error_sink_stream_default = (
    f"mw_page_content_change_enrich.error:{error_schema_version}"
)
"""
Default error_sink EventStreamDescriptor.
"""


arg_parser = ArgumentParser(
    prog="mediawiki-page-content-change-enrichment",
    description="Enriches mediawiki/page/change events with raw wiki content using MW API:Revisions",
)
arg_parser.add_argument(
    "--enrich.mediawiki_api_endpoint_template",
    default=mediawiki_api_endpoint_template_default,
    help="A python format string template for the MW Action API endpoint to use.  "
    "Only `domain` is supported in the template, and will be extracted from the input event.",
)
arg_parser.add_argument(
    "--enrich.ssl_ca_bundle_path",
    help="Path to the SSL CA bundle to use when making HTTPS requests.",
)
arg_parser.add_argument(
    "--enrich.request_timeout",
    type=int,
    default=1,
    help="MW API request timeout in seconds",
)
arg_parser.add_argument(
    "--enrich.max_rev_size",
    type=int,
    default=8_000_000,
    help="Max allowed size (bytes) of a revision `rev_size`. Events whose "
    "payload exceeds max_rev_size won't be enriched",
)
# Since we will be using @http_process_function to decorate
# our http enrich process function, add params for it.
arg_parser.add_function_arguments(
    http_process_function,
    nested_key="http_session",
)

arg_parser_defaults = {
    "enrich.mediawiki_api_endpoint_template": mediawiki_api_endpoint_template_default,
    "stream_manager.job_name": job_name,
    "stream_manager.source.stream": source_stream_default,
    "stream_manager.sink.stream": sink_stream_default,
    "stream_manager.error_sink.stream": error_sink_stream_default,
    "http_session.pool_maxsize": 12,
}


def retry_on_get_page_content_response(
    event: dict,
    http_session: requests.Session,
    user_agent: str,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    wait_on_retry_seconds: int = 5,
    retry_on_event_time_lag_seconds: int = 10,
) -> dict:
    # schema specific access pattern
    revision_id = event["revision"]["rev_id"]
    domain = event["meta"]["domain"]

    # Create a retry predicate closure for this event.
    # This will retry if badrevid response and the event
    # is more recent than retry_on_event_time_lag_seconds.
    retry_predicate = create_retry_predicate_for_event(
        event,
        retry_on_event_time_lag_seconds,
    )

    response = get_page_content_response(
        revision_id=revision_id,
        domain=domain,
        user_agent=user_agent,
        http_session=http_session,
        mediawiki_api_endpoint_template=mediawiki_api_endpoint_template,
        ssl_ca_bundle_path=ssl_ca_bundle_path,
        request_timeout=request_timeout,
        wait_on_retry_seconds=wait_on_retry_seconds,
        retry_predicate=retry_predicate,
    )

    # Raise an HTTPError if http status is not 2xx
    response.raise_for_status()

    response_body = response.json()
    # Handle the common case where the revision was not available from the MW API.
    # This happens when the page or revision is removed soon after the page change
    # event happens, when a database replica is lagging, or when maintenance scripts
    # are executed on a database.
    # In this case we should raise an exception and the event will be forwarded to
    # the error topic.

    if "badrevids" in response_body["query"]:
        log_event_enrich_request_error(event, response, response_body)
        # Raise an exception so the stream manager error handler can emit an error event.
        # NOTE: If error_sink is not enabled, this exception will be raised up the stack
        # and the job will fail.
        raise MediaWikiApiMissingContentError(
            "Failed requesting %s content from %s. Response body:\n%s"
            % (domain, response.request.url, response_body),
        )

    return response_body


def enrich_with_content(event: dict, response_body: dict) -> dict:
    """
    Enrich a page change event with content payload and metadata.

    :param event: a dict representing an event with page_change schema.
    :response_body: the body of a successful MediaWiki (action) api request.
    :return: the input event, enriched with content.
    """
    slots = response_body["query"]["pages"][0]["revisions"][0]["slots"]

    # Iterate through each of the returned revision slots
    # and enrich the page_content_change event.
    for slot_name, slot_data in slots.items():
        if "content" in slot_data:
            event["revision"]["content_slots"][slot_name]["content_body"] = slot_data[
                "content"
            ]
            # TODO: this is specific to mediawiki_content_history enrichment.
            #  Should be refactored out if we adopt a different event schema for that stream.
            if (
                "content_model" not in event["revision"]["content_slots"][slot_name]
                or event["revision"]["content_slots"][slot_name]["content_model"]
                is None
            ):
                event["revision"]["content_slots"][slot_name]["content_model"] = (
                    slot_data["contentmodel"]
                )
            if (
                "content_format" not in event["revision"]["content_slots"][slot_name]
                or event["revision"]["content_slots"][slot_name]["content_format"]
                is None
            ):
                event["revision"]["content_slots"][slot_name]["content_format"] = (
                    slot_data["contentformat"]
                )
    return event


def enrich_page_change_with_content(
    event: dict,
    http_session: requests.Session,
    user_agent: str,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    max_rev_size: int = 8_000_000,
    wait_on_retry_seconds: int = 5,
    retry_on_event_time_lag_seconds: int = 10,
) -> dict:
    assert_allowed_revision_size(event, max_rev_size)

    response_body = retry_on_get_page_content_response(
        event,
        http_session,
        user_agent,
        mediawiki_api_endpoint_template,
        ssl_ca_bundle_path,
        request_timeout,
        wait_on_retry_seconds,
        retry_on_event_time_lag_seconds,
    )

    return enrich_with_content(event, response_body)


def main(config: Namespace = None) -> None:
    config = config or get_config(
        parser=arg_parser, parser_defaults=arg_parser_defaults
    )

    @http_process_function(**config.http_session)
    def enrich(event: dict, http_session: requests.Session) -> dict:
        # Remove meta.id so it can be regenerated from eventutilities.
        # TODO: upstream this operation?
        if "id" in event["meta"]:
            event["meta"].pop("id", "")

        return (
            event
            if event["page_change_kind"] not in ["edit", "create"]
            else enrich_page_change_with_content(
                event=event,
                http_session=http_session,
                user_agent=job_name,
                **config.enrich,
            )
        )

    with stream_manager.from_config(config) as stream:
        # Filter out canary events
        stream.filter(
            lambda e: e["meta"]["domain"] != "canary",
            name="remove_canary_events",
        )

        # The mediawiki.dump.revision_content_history.reconcile.rc0
        # stream is produced by eventgate to Kafka with a JSON blob key that
        # includes page_id and wiki_id.
        # TODO: nowadays stream partitioning info is configured in ESC.
        # we should update eventutilities-python to perform this operation
        # implicitly (for default kafka partition keys).
        stream.partition_by(lambda e: (e["wiki_id"], e["page"]["page_id"]))
        stream.process(enrich, name="enrich_with_page_content")
        stream.execute()


if __name__ == "__main__":
    setup_logging(level=os.environ.get("LOG_LEVEL", "INFO"))
    main()
