import logging

import requests


def get_event_log_labels(event: dict):
    # labels.domain is the wiki domain set in Host header.
    # might differ from `url.full` domain.
    return {
        "labels.domain": event["meta"]["domain"],
        "labels.wiki_id": event["wiki_id"],
        "labels.page_id": event["page"]["page_id"],
        "labels.page_title": event["page"]["page_title"],
        "labels.rev_id": event["revision"]["rev_id"],
    }


def log_event_enrich_request_error(
    event: dict,
    response: requests.Response,
    response_body: dict,
):
    log_labels = get_event_log_labels(event)
    logging.error(
        "Failed requesting %s content from %s",
        event["meta"]["domain"],
        response.request.url,
        extra={
            "http.response.body.content": response_body,
            "url.full": response.request.url,
        }.update(log_labels),
    )
