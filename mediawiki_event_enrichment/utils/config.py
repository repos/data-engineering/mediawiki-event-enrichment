from typing import Any, Dict, List, Optional

from eventutilities_python.stream import load_config
from jsonargparse import ArgumentParser, Namespace


def get_config(
    argv: List[str] = None,
    defaults: Optional[Dict[Any, Any]] = None,
    default_config_files: Optional[List[str]] = None,
    parser: Optional[ArgumentParser] = None,
    parser_defaults: Optional[Dict[Any, Any]] = None,
) -> Namespace:
    defaults = defaults or {}
    parser_defaults = parser_defaults or {}

    combined_defaults = {**parser_defaults, **defaults}

    return load_config(
        argv=argv,
        parser=parser,
        defaults=combined_defaults,
        default_config_files=default_config_files,
    )
