from __future__ import annotations

import logging
from typing import Callable, Optional

import requests
from requests import Response

from mediawiki_event_enrichment.mwapi.action import (
    ContentBodyTooLargeError,
    mediawiki_api_endpoint_template_default,
    mk_url,
    retry_response,
)
from mediawiki_event_enrichment.utils.logging import get_event_log_labels


def page_content_api_url(
    revision_id: int,
    domain: str,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    rvslots: str = "*",
    maxlag: Optional[int] = None,
) -> str:
    """
    Returns the MW API URL for getting revision content.

    :param revision_id:
        Revision for which to get content

    :param domain:
        URL domain to use, e.g. en.wikipedia.org

    :param mediawiki_api_endpoint_template:
        URI prefix template from.  For MW API, this should be a URI that
        includes the {domain} as a format string, up to the api.php part.

    :param rvslots:
        Revision slots for which to get content.  Default * gets all slots.

    :param maxlag:
        MW API maxlag param.  Optional.

    """
    endpoint = mediawiki_api_endpoint_template.format(domain=domain)

    url = (
        f"{endpoint}?action=query&format=json&formatversion=2&prop=revisions&"
        f"revids={revision_id}&rvprop=content&rvslots={rvslots}"
    )
    return mk_url(url, maxlag)


def get_page_content_response(
    revision_id: int,
    domain: str,
    user_agent: str,
    http_session: requests.Session,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    wait_on_retry_seconds: int = 5,
    retry_predicate: Optional[Callable[[Response], bool]] = None,
) -> Response:
    headers = {
        # We must advertise to Mediawiki that this request
        # is non-human traffic (bot).
        "user-agent": f"{user_agent} bot",
        "host": domain,
    }

    url = page_content_api_url(revision_id, domain, mediawiki_api_endpoint_template)

    adapter = http_session.adapters.get("https://")
    # eventutilities-python helpers will instantiate a Retry, but a user could override
    # that by supplying a Session() with different adapter logic. If not an instance
    # of Retry, `adapter.max_retries` will be an int that defaults to 0 if not
    # set by the user.
    max_retries = adapter.max_retries
    if isinstance(max_retries, requests.adapters.Retry):
        max_retries = max_retries.total

    return retry_response(
        http_session,
        url,
        headers=headers,
        verify=ssl_ca_bundle_path,
        timeout=request_timeout,
        max_retries=max_retries,
        wait_on_retry_seconds=wait_on_retry_seconds,
        retry_predicate=retry_predicate,
    )


def assert_allowed_revision_size(event: dict, max_rev_size: int):
    """
    Check that the byte sum of the sizes of all content slots
    in this event will not exceed `enrich.max_rev_size`.

    :param event: original event
    :param max_rev_size: max allowed content size (bytes)
    :return:
    """
    # Ideally we'd want to filter events out early on (e.g. stream.filter(...) in the app
    # context. However, `filter()` currently does not allow redirecting to a side output
    # (the error stream). Raising an exception here guarantees that the originating event
    # will be produced in the error stream.
    #
    # I did consider moving this logic to eventutilities-python, but it's not obvious
    # where a good place would be.
    # We would need to inspect message size before committing to Kafka which means:
    # - we could check size in `EventProcessFunction`: but it's not obvious
    # that we should always drop the message there. Downstream operators might
    # still want to consume large messages before writing into a sink.
    # - before accessing the sink, or serializing Python -> JVM:
    # but we would not be able to forward to a sideoutput (error stream).
    # - in Java eventutilities: this might require having an ad-hoc FlinkProducer,
    # which seems overkill given the number of uses cases we have right now.
    rev_size = int(event["revision"]["rev_size"])
    if not rev_size < max_rev_size:
        logging.error(
            "Revision content body exceeds max allowed size (bytes)",
            extra=get_event_log_labels(event),
        )
        raise ContentBodyTooLargeError(
            f"Enriched event rev_size ({rev_size}) exceeds enrich.max_rev_size ({max_rev_size})"
        )
