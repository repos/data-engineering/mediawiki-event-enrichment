from __future__ import annotations

import logging
import time
from datetime import datetime, timedelta
from typing import Callable, Optional

import requests
from requests import Response

mediawiki_api_endpoint_template_default = "https://{domain}/w/api.php"
"""
Default MW API endpoint.  The default is to request page content from the 'meta.domain'
field host in the page_change event.
"""


class MediaWikiApiMissingContentError(Exception):
    """
    Represents an encountered error or unexpected response from the MediaWiki HTTP API.
    """

    pass


class ContentBodyTooLargeError(Exception):
    """
    The enriched message content exceeds the maximum allowed size (bytes).
    """

    pass


def create_retry_predicate_for_event(
    event: dict,
    retry_on_event_time_lag_seconds=10,
) -> Callable[[Response], bool]:
    """
    Returns a new function that compares the event to be enriched with
    the MW API HTTP response.

    There are cases when Mediawiki returns 200 status on errors.
    See https://phabricator.wikimedia.org/T33156 for an example.
    This method wraps `requests.Session.get`. and `eventutilities-python`
    default retry logic, to allow retry on 200s.

    This function will return True if the MW API http request should be retried.
    https://phabricator.wikimedia.org/T347884#9266842

    :param event:
        Input event dict

    :param retry_on_event_time_lag_seconds:
        If we get badrevid response from the MW API, and processing time (current time) is
        too close to event time by this much, the event should be retried.
    """

    # event["dt"] is a PyFlink Instant.
    # eventutilities-python should probably give this to us as a DateTime.
    # Fix this after https://phabricator.wikimedia.org/T349640 is done.

    # Convert event time to a python datetime.
    # This event_dt is used in the retry_after closure below.
    event_dt = datetime.fromtimestamp(event["dt"].to_epoch_milli() / 1000.0)

    def should_retry(http_response: Response) -> bool:
        """
        retry_predicate function.  If MW API returns 200 and badrevids,
        we might be retrying too early.  MW's database replica might be lagging
        slightly.  Retry and wait if processing time is too close to event time.
        """

        processing_dt = datetime.now()
        if (
            http_response.status_code == 200
            and "badrevids" in http_response.json()["query"]
            and processing_dt - event_dt
            < timedelta(seconds=retry_on_event_time_lag_seconds)
        ):
            logging.info(
                f"Event with meta.id {event['meta']['id']} got badrevid response and should be "
                f"retried. Current processing time {processing_dt} is too close to event "
                f"time {event_dt}. (retry_on_event_time_lag_seconds={retry_on_event_time_lag_seconds})"
            )
            return True
        else:
            return False

    return should_retry


def retry_response(
    http_session: requests.Session,
    url: str,
    headers: dict = None,
    verify: Optional[str] = None,
    timeout: int = 1,
    max_retries: int = 3,
    wait_on_retry_seconds: int = 5,  # TODO: make this a cli / config option
    retry_predicate: Optional[Callable[[Response], bool]] = None,
):
    """
    Attempts and then conditionally retries a request.

    :param http_session: an instance of `requests.Session`
    :param url: url to retrieve
    :param headers: headers to be sent
    :param verify: SSL verification (ca bundle path)
    :param timeout: HTTP request timeout
    :param max_retries: max number of retries when the requests fails
    :param wait_on_retry_seconds: time (in seconds) to wait between retries
    :param retry_predicate:
        An optional callable that takes the response and determines if the request should be retried.
        This function should likely not retry on the usual bad HTTP error response codes,
        as that is handled by default retry logic.  Only provide this if you need
        to conditionally retry based on a 'good' HTTP response code.
    :return:
    """
    response = http_session.get(
        url,
        headers=headers,
        verify=verify,
        timeout=timeout,
    )

    if retry_predicate and retry_predicate(response) and max_retries > 0:
        logging.info(
            f"Sleeping {wait_on_retry_seconds} and retrying request to {url}. "
            f"Retries left: {max_retries - 1}. Response:\n{response}"
        )
        time.sleep(wait_on_retry_seconds)
        return retry_response(
            http_session,
            url,
            headers,
            verify,
            timeout,
            max_retries - 1,
            wait_on_retry_seconds,
        )
    return response


def mk_url(url, maxlag: Optional[int] = None):
    """ """
    if maxlag:
        url = url + f"&maxlag={maxlag}"
    return url
