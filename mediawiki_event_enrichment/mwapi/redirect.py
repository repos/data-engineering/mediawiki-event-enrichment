from typing import Callable, Optional

import requests
from requests import Response

from mediawiki_event_enrichment.mwapi.action import (
    mediawiki_api_endpoint_template_default,
    mk_url,
    retry_response,
)


def page_redirect_api_url(
    page_id: int,
    domain: str,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    rvslots: str = "*",
    maxlag: Optional[int] = None,
) -> str:
    """
    Returns the MW API URL for getting page redirects from a page.

    :param page_id:
        Page for which to get redirect info

    :param domain:
        URL domain to use, e.g. en.wikipedia.org

    :param mediawiki_api_endpoint_template:
        URI prefix template from.  For MW API, this should be a URI that
        includes the {domain} as a format string, up to the api.php part.

    :param rvslots:
        Revision slots for which to get content.  Default * gets all slots.

    :param maxlag:
        MW API maxlag param.  Optional.

    """
    endpoint = mediawiki_api_endpoint_template.format(domain=domain)
    url = f"{endpoint}?action=query&format=json&formatversion=2&prop=revisions&pageids={page_id}&rvslots={rvslots}&redirects=1"
    return mk_url(url, maxlag)


def get_page_redirect_response(
    page_id: int,
    domain: str,
    user_agent: str,
    http_session: requests.Session,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    wait_on_retry_seconds: int = 5,
    retry_predicate: Optional[Callable[[Response], bool]] = None,
) -> Response:
    headers = {
        # We must advertise to Mediawiki that this request
        # is non-human traffic (bot).
        "user-agent": f"{user_agent} bot",
        "host": domain,
    }

    url = page_redirect_api_url(page_id, domain, mediawiki_api_endpoint_template)

    adapter = http_session.adapters.get("https://")
    # eventutilities-python helpers will instantiate a Retry, but a user could override
    # that by supplying a Session() with different adapter logic. If not an instance
    # of Retry, `adapter.max_retries` will be an int that defaults to 0 if not
    # set by the user.
    max_retries = adapter.max_retries
    if isinstance(max_retries, requests.adapters.Retry):
        max_retries = max_retries.total

    return retry_response(
        http_session,
        url,
        headers=headers,
        verify=ssl_ca_bundle_path,
        timeout=request_timeout,
        max_retries=max_retries,
        wait_on_retry_seconds=wait_on_retry_seconds,
        retry_predicate=retry_predicate,
    )
