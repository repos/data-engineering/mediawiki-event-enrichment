
install_runtime_deps:
	git submodule update --init

tests:
	pytest --cov-config=.coveragerc

.PHONY: install_runtime_deps tests
