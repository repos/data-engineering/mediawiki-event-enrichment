import json
from datetime import datetime, timedelta
from uuid import uuid4

import pytest
import requests
from eventutilities_python.testing.utils import \
    read_records_from_flink_row_file_sink
from pyflink.common import Instant
from pytest_httpserver import HTTPServer

from mediawiki_event_enrichment.mwapi.content import \
    assert_allowed_revision_size
from mediawiki_event_enrichment.page_content_change import (
    ContentBodyTooLargeError, arg_parser, arg_parser_defaults,
    create_retry_predicate_for_event, main)
from mediawiki_event_enrichment.utils.config import get_config


def test_max_event_size():
    event = {
        "wiki_id": "test_data",
        "meta": {"domain": "test_data"},
        "page": {"page_title": "test_data", "page_id": "page_title"},
        "revision": {"rev_size": 216, "rev_id": "test_data"},
    }

    with pytest.raises(ContentBodyTooLargeError):
        assert_allowed_revision_size(event, 100)

    assert_allowed_revision_size(event, 10_000_000_000)


def test_create_retry_predicate_for_event(
    httpserver: HTTPServer,
):
    processing_dt = datetime.now()

    recent_dt = processing_dt - timedelta(seconds=2)
    old_dt = processing_dt - timedelta(hours=24)

    rev_id = 555

    event_recent = {
        "wiki_id": "test_data",
        "meta": {"domain": "test_data", "id": uuid4()},
        "page": {"page_title": "test_data", "page_id": "page_title"},
        "revision": {"rev_size": 216, "rev_id": rev_id},
        # eventutilities-python should probably give this to us as a DateTime.
        # Fix this after https://phabricator.wikimedia.org/T349640 is done.
        "dt": Instant.of_epoch_milli(int(recent_dt.timestamp() * 1000.0)),
    }

    event_old = {
        "wiki_id": "test_data",
        "meta": {"domain": "test_data", "id": uuid4()},
        "page": {"page_title": "test_data", "page_id": "page_title"},
        "revision": {"rev_size": 216, "rev_id": rev_id},
        # eventutilities-python should probably give this to us as a DateTime.
        # Fix this after https://phabricator.wikimedia.org/T349640 is done.
        "dt": Instant.of_epoch_milli(int(old_dt.timestamp() * 1000.0)),
    }

    bad_response = {
        "query": {"badrevids": {str(rev_id): {"revid": rev_id, "missing": True}}}
    }

    query_string = f"action=query&format=json&formatversion=2&prop=revisions&revids={rev_id}&rvprop=content&rvslots=*"
    httpserver.expect_request("/", query_string=query_string).respond_with_json(
        bad_response
    )

    url = httpserver.url_for("") + "?" + query_string
    response = requests.get(url)

    assert (
        create_retry_predicate_for_event(
            event_recent,
            retry_on_event_time_lag_seconds=10,
        )(response)
        is True
    ), "A recent event with a badrevid response should be retried."

    assert (
        create_retry_predicate_for_event(
            event_old,
            retry_on_event_time_lag_seconds=10,
        )(response)
        is False
    ), "An old event with a badrevid response should not be retried."


@pytest.mark.parametrize(
    "fixture_stream_manager_defaults",
    {"fixture_page_change_events_files": None},  # {fixture: input}
    indirect=True,
)
def test_page_content_change_enrich(
    fixture_stream_manager_default_config_files,
    fixture_stream_manager_defaults,
    httpserver: HTTPServer,
    tmp_path: str,
):
    config = get_config(
        argv=[],
        defaults=fixture_stream_manager_defaults
        | {
            "enrich.mediawiki_api_endpoint_template": httpserver.url_for(""),
            "enrich.max_rev_size": 2_000_000,
        },
        default_config_files=fixture_stream_manager_default_config_files,
        parser=arg_parser,
        parser_defaults=arg_parser_defaults,
    )

    good_response = {
        "query": {
            "pages": [{"revisions": [{"slots": {"main": {"content": "Test content"}}}]}]
        }
    }
    bad_response = {
        "query": {"badrevids": {"2147483647": {"revid": 2147483647, "missing": True}}}
    }

    query_string = "action=query&format=json&formatversion=2&prop=revisions&revids={rev_id}&rvprop=content&rvslots=*"
    httpserver.expect_request(
        "/", query_string=query_string.format(rev_id=3270050)
    ).respond_with_json(good_response)
    httpserver.expect_request(
        "/", query_string=query_string.format(rev_id=3270051)
    ).respond_with_json(good_response)
    httpserver.expect_request(
        "/", query_string=query_string.format(rev_id=2147483647)
    ).respond_with_json(bad_response)

    main(config)

    # Read successful and errored events from the configured test file sinks.
    enriched_events = read_records_from_flink_row_file_sink(config.stream_manager.sink)
    # 5 input events, one canary (filtered out) and three errors (written to the error sink).
    assert len(enriched_events) == 2
    for event in enriched_events:
        assert "content_body" in event["revision"]["content_slots"]["main"]
        # meta.id should not be the same as the input events
        assert event["meta"]["id"] not in (
            "1ebb5411-5f86-408a-9324-390f626ddca4",
            "e9b4a51c-a7b9-48f6-8ab4-cbdc14eddacf",
        )

    error_events = read_records_from_flink_row_file_sink(
        config.stream_manager.error_sink
    )
    assert len(error_events) == 2
    expected_error_rev_ids = {2147483647, 12346}
    error_rev_ids = set()
    for event in error_events:
        errored_event = json.loads(event["raw_event"])
        error_rev_ids.add(errored_event["revision"]["rev_id"])
        if errored_event["revision"]["rev_id"] == 2147483647:
            assert event["error_type"] == "MediaWikiApiMissingContentError"
        elif errored_event["revision"]["rev_id"] == 12346:
            assert event["error_type"] == "ContentBodyTooLargeError"

    assert expected_error_rev_ids == error_rev_ids
