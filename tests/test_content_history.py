import json

import pytest
from eventutilities_python.testing.utils import read_records_from_flink_row_file_sink
from pytest_httpserver import HTTPServer

from mediawiki_event_enrichment.content_history import (
    arg_parser,
    arg_parser_defaults,
    main,
)
from mediawiki_event_enrichment.utils.config import get_config


@pytest.mark.parametrize(
    "fixture_stream_manager_defaults",
    {"fixture_content_history_events_files": None},  # {fixture: input}
    indirect=True,
)
def test_content_history_enrich(
    fixture_stream_manager_default_config_files,
    fixture_stream_manager_defaults,
    httpserver: HTTPServer,
    tmp_path: str,
):
    config = get_config(
        argv=[],
        defaults=fixture_stream_manager_defaults
        | {
            "enrich.mediawiki_api_endpoint_template": httpserver.url_for(""),
            "enrich.max_rev_size": 2_000_000,
        },
        default_config_files=fixture_stream_manager_default_config_files,
        parser=arg_parser,
        parser_defaults=arg_parser_defaults,
    )

    good_page_content_response = {
        "query": {
            "pages": [
                {
                    "revisions": [
                        {
                            "slots": {
                                "main": {
                                    "content": "Redirected page content.",
                                    "contentmodel": "wikitext",
                                    "contentformat": "text/x-wiki",
                                }
                            }
                        }
                    ]
                }
            ]
        }
    }

    good_page_redirect_response = {
        "batchcomplete": "",
        "query": {
            "redirects": [{"from": "Old page", "to": "New page"}],
            "pages": [
                {
                    "pageid": 322194,
                    "ns": 0,
                    "title": "New page",
                    "revisions": [
                        {
                            "revid": 11223344,
                            "user": "some user",
                            "timestamp": "2024-08-21T04:36:56Z",
                            "comment": "merge history, added sources",
                        }
                    ],
                }
            ],
        },
    }

    bad_page_content_response = {
        "query": {"badrevids": {"2147483647": {"revid": 2147483647, "missing": True}}}
    }

    # page_id : 322194,
    page_content_query_string = "action=query&format=json&formatversion=2&prop=revisions&revids={rev_id}&rvprop=content&rvslots=*"
    page_redirect_query_string = "action=query&format=json&formatversion=2&prop=revisions&pageids={page_id}&rvslots=*&redirects=1"

    httpserver.expect_request(
        "/", query_string=page_content_query_string.format(rev_id=3270051)
    ).respond_with_json(good_page_content_response)

    httpserver.expect_request(
        "/", query_string=page_redirect_query_string.format(page_id=322194)
    ).respond_with_json(good_page_redirect_response)

    httpserver.expect_request(
        "/", query_string=page_content_query_string.format(rev_id=2147483647)
    ).respond_with_json(bad_page_content_response)

    httpserver.expect_request(
        "/", query_string=page_redirect_query_string.format(page_id=322195)
    ).respond_with_data("Bad Request", status=400)

    main(config)

    # Read successful and errored events from the configured test file sinks.
    enriched_events = read_records_from_flink_row_file_sink(config.stream_manager.sink)

    assert len(enriched_events) == 1

    event = enriched_events.pop()
    assert event["revision"]["rev_id"] == 3270051
    assert event["page"]["is_redirect"] == True
    assert "created_redirect_page" in event
    # this app should not set is_redirect
    # See: https://gitlab.wikimedia.org/repos/data-engineering/mediawiki-event-enrichment/-/merge_requests/93#note_118195
    assert "is_redirect" not in event["created_redirect_page"]
    assert event["created_redirect_page"]["page_id"] == 322194
    assert event["created_redirect_page"]["page_title"] == "New page"
    assert event["created_redirect_page"]["namespace_id"] == 0

    assert "content_body" in event["revision"]["content_slots"]["main"]
    assert (
        event["revision"]["content_slots"]["main"]["content_body"]
        == "Redirected page content."
    )
    # content_model and content_format are not present in the fixture, and should
    # by added by a mwapi call.
    assert event["revision"]["content_slots"]["main"]["content_model"] == "wikitext"
    assert event["revision"]["content_slots"]["main"]["content_format"] == "text/x-wiki"

    error_events = read_records_from_flink_row_file_sink(
        config.stream_manager.error_sink
    )
    assert len(error_events) == 2

    expected_error_rev_ids = {2147483647}
    expected_error_page_ids = {322195, 322194}
    error_rev_ids = set()
    error_page_ids = set()

    for event in error_events:
        errored_event = json.loads(event["raw_event"])
        error_rev_ids.add(errored_event["revision"]["rev_id"])
        error_page_ids.add(errored_event["page"]["page_id"])
        # content request failed.
        if (
            errored_event["revision"]["rev_id"] == 2147483647
            and errored_event["page"]["page_id"] == 322194
        ):
            assert event["error_type"] == "MediaWikiApiMissingContentError"
        # both requests failed. event["error_type"] should be set by the future
        # that completed last.
        if (
            errored_event["revision"]["rev_id"] == 2147483647
            and errored_event["page"]["page_id"] == 322195
        ):
            assert event["error_type"] in {
                "MediaWikiApiMissingContentError",
                "HTTPError",
            }

    assert expected_error_rev_ids == error_rev_ids
    assert expected_error_page_ids == error_page_ids
