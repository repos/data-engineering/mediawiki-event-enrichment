import json
import os
from pathlib import Path
from typing import Any, Dict, List

import pytest

# FIXME: I randomly bumped into  https://github.com/apache/flink/pull/17239. Local flink issue?
os.environ["_python_worker_execution_mode"] = "process"


pwd = Path(__file__).parent.resolve()
tests_path = f"{pwd}/tests"


def load_event_files(files: List[str]) -> List[Dict[str, Any]]:
    events = []
    for f in files:
        with open(f, "r") as fp:
            events = events + [json.loads(d) for d in fp.readlines()]
    return events


@pytest.fixture(scope="session")
def fixture_page_change_events_files():
    return [os.path.join(tests_path, "fixtures", "fixture_page_change_events.json")]


@pytest.fixture(scope="session")
def fixture_page_change_events(fixture_page_change_events_files):
    return load_event_files(fixture_page_change_events_files)

@pytest.fixture(scope="session")
def fixture_content_history_events_files():
    return [os.path.join(tests_path, "fixtures", "fixture_mediawiki_content_history.json")]

@pytest.fixture
def fixture_stream_manager_default_config_files():
    """
    Returns a config file path for use with all python tests.
    """
    return [os.path.join(tests_path, "config", "config.test.defaults.yaml")]


@pytest.fixture
def fixture_stream_manager_defaults(tmp_path, request):
    """
    Returns a dict of jsonargparse defaults for stream_manager to use
    in tests.  Most importantly the input and output uri paths
    are generated dynamically in tmp_path.

    :param request: an instance of pytest.fixture.
        We pass this fixture here to parametrize tests with
        different nested event fixture JSON files..
    """

    # Use git submodule of schema repo for tests.
    project_root_path = os.path.dirname(os.path.abspath(__file__))
    schema_uris = [
        os.path.join("file://", project_root_path, "event-schemas", "primary", "jsonschema"),
    ]

    fixture_event_files = request.param

    return {
        "stream_manager.schema_uris": schema_uris,
        "stream_manager.source.options": {"uris": request.getfixturevalue(fixture_event_files)},
        "stream_manager.sink.options": {"uri": os.path.join(tmp_path, "output")},
        "stream_manager.error_sink.options": {"uri": os.path.join(tmp_path, "errors")},
    }
